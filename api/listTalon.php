<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('iblock');
global $USER;
if ($USER->IsAuthorized()) {

    if (!empty($_REQUEST['show_employee'])) {
        include_once(__DIR__ . "/xlsxwriter.class.php");
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);
        $filename = date("d_m_Y") . "_export.xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $ar_filter = [
            'TALON_DATE' => date("Y-m-d"),
            'EMPLOYEE' => $_REQUEST['show_employee'],
            '<STATE' => '3',
        ];

        $tm_talon = MCTalon::GetList($ar_filter);

        $new = array_multisort($tm_talon, SORT_ASC);

        $field = 'TIME_START';

        $sortArr = array();
        foreach($tm_talon as $key => $val){
            $sortArr[$key] = $val[$field];
        }

        array_multisort($sortArr, $tm_talon);
        $styles1 = array(
            'height' => 50,
            'widths' => ['1' => 20],
            'font' => 'Arial',
            'font-size' => 10,
            'font-style' => 'bold',
            'fill' => '#eee',
            'halign' => 'center',
            'border' => 'left,right,top,bottom',
            'wrap_text' => true,
            'alignment' => true);

        $styles_header = array(
            'height' => 50,
            'widths' => ['1' => 20],
            'font' => 'Arial',
            'font-size' => 15,
            'font-style' => 'bold',
            'fill' => '#eee',
            'halign' => 'center',
            'border' => 'left,right,top,bottom',
            'wrap_text' => true,
            'alignment' => true);

        foreach ($tm_talon as $idTalona => $dataTalona) {

            $rsUser = CUser::GetByID($dataTalona['EMPLOYEE']);
            $arUser = $rsUser->Fetch();

            $dataTalona['EMPLOYEE'] = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'];

            $ar_filter = array('ID' => $dataTalona['USER_INFO']);
            $ar_inf = MCUser::GetUserInfo($ar_filter);

            $userDara = array_shift($ar_inf);
            foreach ($userDara['PROPERTIES'] as $name => $value) {
                if ($value['VALUE']) {
                    $dataTalona[$value['NAME']] = $value['VALUE'];
                }
            }

            $rsService = CIBlockElement::GetList(array(), array('ID' => $dataTalona['SERVICE']), false, false, array('NAME'));
            $d = $rsService->GetNext();
            $dataTalona['SERVICE'] = $d['NAME'];


            $flr = array('ID' => $dataTalona['ORGANIZATION']);
            $arSelect = Array("ID", "NAME", "PROPERTY_ADRESS", 'PROPERTY_PHONE');
            $rs = CIBlockElement::GetList(Array(), $flr, false, false, $arSelect);
            $t_value = $rs->GetNext();
            $dataTalona['ORGANIZATION'] = $t_value['NAME'];

            if (!empty($dataTalona['TIME_START']) && !empty($dataTalona['TIME_END'])) {
                $dt = strtotime($dataTalona['TIME_START']);
                $dte = strtotime($dataTalona['TIME_END']);

                $dataTalona['TIME_START'] = date('H:i', $dt);
                $dataTalona['TIME_END'] = date('H:i', $dte);
            }

            $dataTalona['№ талона'] = $dataTalona['ID'];
            $dataTalona['Дата приема'] = $dataTalona['DATE'];
            $dataTalona['Время'] = $dataTalona['TIME_START'] . '-' . $dataTalona['TIME_END'];
            $dataTalona['Услуга'] = $dataTalona['SERVICE'];
            $dataTalona['Медицинский центр'] = $dataTalona['ORGANIZATION'];
            $dataTalona['ФИО'] = $dataTalona['Фамилия'] . ' ' . $dataTalona['Имя'] . ' ' . $dataTalona['Отчество'];
            $dataTalona['Телефон '] = $dataTalona['Телефон'];
            $dataTalona['E-Mail '] = $dataTalona['E-Mail'];
            $dataTalona['Адрес '] = $dataTalona['Адрес'];
            $idSpecial['Врач'] = $dataTalona['EMPLOYEE'];

            unset($dataTalona['Фамилия'], $dataTalona['Имя'], $dataTalona['Отчество'], $dataTalona['Телефон'], $dataTalona['Адрес'], $dataTalona['E-Mail']);

            unset($dataTalona['ID'], $dataTalona['DATE'], $dataTalona['EMPLOYEE'], $dataTalona['USER_INFO'], $dataTalona['TIME_START'], $dataTalona['TIME_END'], $dataTalona['ORGANIZATION'], $dataTalona['PAYED'], $dataTalona['SITE_ID'], $dataTalona['EXPORTED'],
                $dataTalona['RESERVED'], $dataTalona['STATE'], $dataTalona['CREATED'], $dataTalona['PRICE'],
                $dataTalona['CURRENCY'], $dataTalona['COMMENT'], $dataTalona['VIDEO'], $dataTalona['TYPE'],
                $dataTalona['type_name'], $dataTalona['PRINT_INT_PRICE'], $dataTalona['SERVICE']);

            $data[$idSpecial['Врач']][$dataTalona['Дата приема'] . '_' . $dataTalona['№ талона']] = $dataTalona;

        }

        foreach ($data as $name => $datum) {

            foreach ($datum as $id => $d) {
                $a = explode('_', $id);
                $t[$a['0']][] = $d;
            }
            ksort($t);


            $writer = new XLSXWriter();
            foreach ($data as $special => $arData) {
                $writer->writeSheetHeader($special, array($special => 'string'), $styles_header);
                $writer->markMergedCell($special, $start_row = 0, $start_col = 0, $end_row = 1, $end_col = 4); //обьединение
                $writer->writeSheetRow($special, array(''), $styles1);
                $writer->writeSheetRow($special,
                    ['№ талона', 'Дата приема', 'Время', 'Услуга', 'Медицинский центр', 'ФИО', 'Телефон', 'e-mail', 'Адрес'], $styles1);
                foreach ($t as $date => $key) {
                    foreach ($key as $idTalon => $dataTalon) {
                        $writer->writeSheetRow($special, $dataTalon, $styles1);
                    }
                }
            }

        }
        $writer->writeToStdOut();
        exit(0);
    }

    if ($_REQUEST['show'] == 'all') {

        include_once(__DIR__ . "/xlsxwriter.class.php");
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);
        $filename = date("d_m_Y") . "_export.xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $date_from = $_REQUEST['date_from'];
        $date_to = $_REQUEST['date_to'];

        $timestamp_date_from = strtotime($date_from);
        $timestamp_date_to = strtotime($date_to);

        $date_from = date("Y-m-d", $timestamp_date_from);
        $date_to = date("Y-m-d", $timestamp_date_to);

        if ($date_to && $date_from) {
            $ar_filter = array(
                '>=TALON_DATE' => $date_from,
                '<=TALON_DATE' => $date_to,
                '<STATE' => '3',
            );
        } else {
            $ar_filter = array();
        }

        $tm_talon = MCTalon::GetList($ar_filter);

        foreach ($tm_talon as $idTalona => $dataTalona) {

            $rsUser = CUser::GetByID($dataTalona['EMPLOYEE']);
            $arUser = $rsUser->Fetch();

            $dataTalona['EMPLOYEE'] = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'];

            $ar_filter = array('ID' => $dataTalona['USER_INFO']);
            $ar_inf = MCUser::GetUserInfo($ar_filter);

            $userDara = array_shift($ar_inf);
            foreach ($userDara['PROPERTIES'] as $name => $value) {
                if ($value['VALUE']) {
                    $dataTalona[$value['NAME']] = $value['VALUE'];
                }
            }

            $rsService = CIBlockElement::GetList(array(), array('ID' => $dataTalona['SERVICE']), false, false, array('NAME'));
            $d = $rsService->GetNext();
            $dataTalona['SERVICE'] = $d['NAME'];


            $flr = array('ID' => $dataTalona['ORGANIZATION']);
            $arSelect = Array("ID", "NAME", "PROPERTY_ADRESS", 'PROPERTY_PHONE');
            $rs = CIBlockElement::GetList(Array(), $flr, false, false, $arSelect);
            $t_value = $rs->GetNext();
            $dataTalona['ORGANIZATION'] = $t_value['NAME'];

            if (!empty($dataTalona['TIME_START']) && !empty($dataTalona['TIME_END'])) {
                $dt = strtotime($dataTalona['TIME_START']);
                $dte = strtotime($dataTalona['TIME_END']);

                $dataTalona['TIME_START'] = date('H:i', $dt);
                $dataTalona['TIME_END'] = date('H:i', $dte);
            }
            $dataTalona['№ талона'] = $dataTalona['ID'];
            $dataTalona['Дата приема'] = $dataTalona['DATE'];
            $dataTalona['Время'] = $dataTalona['TIME_START'] . '-' . $dataTalona['TIME_END'];
            $dataTalona['Услуга'] = $dataTalona['SERVICE'];
            $dataTalona['Медицинский центр'] = $dataTalona['ORGANIZATION'];
            $dataTalona['ФИО'] = $dataTalona['Фамилия'] . ' ' . $dataTalona['Имя'] . ' ' . $dataTalona['Отчество'];
            $dataTalona['Телефон '] = $dataTalona['Телефон'];
            $dataTalona['E-Mail '] = $dataTalona['E-Mail'];
            $dataTalona['Адрес '] = $dataTalona['Адрес'];
            $idSpecial['Врач'] = $dataTalona['EMPLOYEE'];

            unset($dataTalona['Фамилия'], $dataTalona['Имя'], $dataTalona['Отчество'], $dataTalona['Телефон'], $dataTalona['Адрес'], $dataTalona['E-Mail']);

            unset($dataTalona['ID'], $dataTalona['DATE'], $dataTalona['EMPLOYEE'], $dataTalona['USER_INFO'], $dataTalona['TIME_START'], $dataTalona['TIME_END'], $dataTalona['ORGANIZATION'], $dataTalona['PAYED'], $dataTalona['SITE_ID'], $dataTalona['EXPORTED'],
                $dataTalona['RESERVED'], $dataTalona['STATE'], $dataTalona['CREATED'], $dataTalona['PRICE'],
                $dataTalona['CURRENCY'], $dataTalona['COMMENT'], $dataTalona['VIDEO'], $dataTalona['TYPE'],
                $dataTalona['type_name'], $dataTalona['PRINT_INT_PRICE'], $dataTalona['SERVICE']);

            $data[$idSpecial['Врач']][$idTalona] = $dataTalona;

        }

        $styles1 = array(
            'height' => 50,
            'widths' => ['1' => 20],
            'font' => 'Arial',
            'font-size' => 10,
            'font-style' => 'bold',
            'fill' => '#eee',
            'halign' => 'center',
            'border' => 'left,right,top,bottom',
            'wrap_text' => true,
            'alignment' => true);

        $styles_header = array(
            'height' => 50,
            'widths' => ['1' => 20],
            'font' => 'Arial',
            'font-size' => 15,
            'font-style' => 'bold',
            'fill' => '#eee',
            'halign' => 'center',
            'border' => 'left,right,top,bottom',
            'wrap_text' => true,
            'alignment' => true);

        $writer = new XLSXWriter();
        foreach ($data as $special => $arData) {
            $writer->writeSheetHeader($special, array($special => 'string'), $styles_header);
            $writer->markMergedCell($special, $start_row = 0, $start_col = 0, $end_row = 1, $end_col = 4); //обьединение
            $writer->writeSheetRow($special, array(''), $styles1);
            $writer->writeSheetRow($special,
                ['№ талона', 'Дата приема', 'Время', 'Услуга', 'Медицинский центр', 'ФИО', 'Телефон', 'e-mail', 'Адрес'], $styles1);
            foreach ($arData as $idTalon => $dataTalon) {
                $writer->writeSheetRow($special, $dataTalon, $styles1);
            }
        }

        $writer->writeToStdOut();
        exit(0);
    }

    /*
     * создание бэкапа
     */
    if ($_REQUEST['backups'] == 'Y') {

        $ar_filter = array('TALON_DATE' => date("Y-m-d"));
        $tm_talon = MCTalon::GetList($ar_filter);

        foreach ($tm_talon as $idTalona => $dataTalona) {
            $insert .= "INSERT INTO `m_talon` ('ID', 'USER_INFO','EMPLOYEE', 'ORGANIZATION', 'SERVICE', 'SITE_ID', 'EXPORTED', 'RESERVED', 'TIME_START', 'TIME_END', 'STATE', 'DATE', 'CREATED', 'PAYED', 'PRICE', 'CURRENCY', 'COMMENT', 'VIDEO', 'TYPE', 'type_name', 'PRINT_INT_PRICE') VALUES ( '" . $dataTalona['ID'] . "' , '" . $dataTalona['USER_INFO'] . "' , '" .
                $dataTalona['EMPLOYEE'] . "' , '" .
                $dataTalona['ORGANIZATION'] . "' , '" . $dataTalona['SERVICE'] . "' , '" .
                $dataTalona['SITE_ID'] . "' , '" . $dataTalona['EXPORTED'] . "' , '" . $dataTalona['RESERVED'] . "' , '" .
                $dataTalona['TIME_START'] . "' ,'" . $dataTalona['TIME_END'] . "' ,'" . $dataTalona['STATE'] . "' ,'" .
                $dataTalona['DATE'] . "' , '" . $dataTalona['CREATED'] . "' , '" . $dataTalona['PAYED'] . "' , '" .
                $dataTalona['PRICE'] . "' , '" . $dataTalona['CURRENCY'] . "' , '" . $dataTalona['COMMENT'] . "' , '" .
                $dataTalona['VIDEO'] . "' , '" . $dataTalona['TYPE'] . "' , '" . $dataTalona['type_name'] . "' , '" .
                $dataTalona['PRINT_INT_PRICE'] . "' );\r";
        }
        $tables = 'm_talon';
        $handle = fopen('db-backup-' . date("d.m.y") . '.sql', 'w+');
        fwrite($handle, $insert);
        fclose($handle);
    }
}
