<?
include_once 'include/constants.php';

require($_SERVER["DOCUMENT_ROOT"] . "/tools/amoCRM.php");

function pr($a) {
    echo "<pre>"; print_r($a); echo "</pre>";
}

/*работает и всюду используется эта функция*/
function post_content ($url, $postdata) {
    $uagent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)";

    $ch = curl_init( $url );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, "application/json");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata));
    curl_setopt($ch, CURLOPT_COOKIEJAR, "z://coo.txt");
    curl_setopt($ch, CURLOPT_COOKIEFILE,"z://coo.txt");

    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );

    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    $d = $header;

    $out = substr($header['content'], 0, 200);
    logerErrorTiket($url, json_encode($postdata), $out);

    if (CANCEL_URL == $url || CANCEL_URL_NATIVE == $url)
    {
        // include api tickets class
        include_once("{$_SERVER['DOCUMENT_ROOT']}/lib/api/tickets.php");

        // split booking
        Tickets::splitBookingCancell($postdata);
    }

    return $d;
}

/*только для файла мирквестов бронь*/
function post_content_ch ($url, $postdata)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json; charset=utf-8', 'Content-Length: ' . strlen($postdata) ));

    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    $d = $header;

    if (CANCEL_URL == $url || CANCEL_URL_NATIVE == $url)
    {
        // include api tickets class
        include_once("{$_SERVER['DOCUMENT_ROOT']}/lib/api/tickets.php");

        // split booking
        Tickets::splitBookingCancell($postdata);
    }

    return $d;
}


if(
$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone")||
    $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android")||
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS")||
            $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry")||
                $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod")||
                    $mobile = strpos($_SERVER['HTTP_USER_AGENT'],"Mobile")||
                        $symb = strpos($_SERVER['HTTP_USER_AGENT'],"Symbian")||
                            $operam = strpos($_SERVER['HTTP_USER_AGENT'],"Opera M")||
                                $htc = strpos($_SERVER['HTTP_USER_AGENT'],"HTC_")||
                                    $fennec = strpos($_SERVER['HTTP_USER_AGENT'],"Fennec/")||
                                        $winphone = strpos($_SERVER['HTTP_USER_AGENT'],"WindowsPhone")||
                                            $wp7 = strpos($_SERVER['HTTP_USER_AGENT'],"WP7")||
                                                $wp8 = strpos($_SERVER['HTTP_USER_AGENT'],"WP8")
){
    define('PDA','Y');
}

function get_price_arena($date, $filmId, $theaterId, $theaterMID)
    /* цены на сегодня filmId для Арены = 4642, вообще ИД для фильма в Тикетах */
{

    $film_params = array("cityId" => CITY_ID,
        "theaterId" => $theaterId,
        "mid" => $theaterMID,
        "date" => (string)$date, /* Y/m/d */
        "filmId" => $filmId
    );

    if($json = post_content(
        "URL_API",
        $film_params
    )){
        $prices_arr = [];
        if ($arr_data[$filmId] = json_decode($json['content'], true)) {
            foreach ($arr_data[$filmId]['schedule'] as $halls) {
                foreach ($halls['prices'] as $price) {
                    $prices_arr[] = $price['price'];
                }
            }
        }
        return $PRICES_UNIQUE = array_unique($prices_arr);
    }
    else{
        return "Нет связи с Тикетс";
    }
}
function isWeekend($date) {
    return (date('N', strtotime($date)) >= 6);
}

function getTimeTable($quest_id)
{
    /**
     * Получение информации по квесту
     */
    $quest_id = intval($quest_id);
    $Quest = [];
    CModule::IncludeModule('iblock');
    $arSelect = Array("ID", "DATE_ACTIVE_FROM", "PROPERTY_HREF_SCHEDULE", "PROPERTY_SOON", "PROPERTY_MID", "PROPERTY_THEATERID");
    $arFilter = Array("IBLOCK_ID"=>QUESTS, "ID"=>$quest_id, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement())
    {
        $Quest = $ob->GetFields();
    }

    global $DB;

    $bron = [];

    $date_q = date('Y-m-d H:i:s',time() - 60);
    $res = $DB->Query("SELECT z.UF_USERID, z.pin FROM zapis z WHERE z.used = {$quest_id} AND z.UF_DATEREC >= '{$date_q}'");
    while($row = $res->Fetch()) {
        $bron[intval($row["UF_USERID"])] = intval($row["pin"]);
    }


    $date = date("Y/m/d");
    if ($Quest['PROPERTY_SOON_VALUE'] <> "Да") {
        if ($Quest['PROPERTY_HREF_SCHEDULE_VALUE']) {

            $line = [];

            /**
             * Даты получения расписания - 14 дней
             */
            $dates = [];
            for($d = strtotime($date); $d<(strtotime($date) + 60*60*24*14); $d = $d + 60*60*24) {
                $dates[] = date("Y/m/d", $d);
            }

            /**
             * Получение расписания
             */
            $film_params = array(
                "cityId" => CITY_ID,
                "theaterId" => $Quest['PROPERTY_THEATERID_VALUE'],
                "mid" => $Quest['PROPERTY_MID_VALUE'],
                "date" => $dates,
                "filmId" => $Quest['PROPERTY_HREF_SCHEDULE_VALUE']
            );


            $json = post_content(
                SCHEDULE_URL_NATIVE,
                $film_params
            );

            $arr_data = json_decode($json['content'], true);
            $schedule = [];
            if (is_array($arr_data)) {
                $schedule = $arr_data['schedule'];
            } else if ($arr_error = json_decode($json['errmsg'])) {
                $arError[] = [$arr_error];
            } else {
                $arError[] = ["No json from Tickets"];
            }

            /**
             * Готовим данные для ответа  -- сделал доп проверку на то что сеанс квеста в прошлом + 30 мин на закрытие бронирования
             */

            foreach ($schedule as $sh) {

                if( isset( $bron[$sh["id"]] ) ) {
                    if( $bron[$sh["id"]] == 0 ) {
                        $is_fr = false;
                    } else {
                        $is_fr = true;
                    }
                } else {
                    if( $sh['seatsFree'] == 0 ){
                        $is_fr = false;
                    } else {
                        $is_fr = true;
                    }
                }

                array_push($line, array(
                        "date" => date("Y-m-d", strtotime($sh['date'])),
                        "time" => date("H:i", strtotime($sh['date'])),
                        "is_free" => ($sh['seatsFree']>0 && $sh['isBook'] == true ) ? true : false,
                        "available" => (isset($bron[$sh["id"]])) ? $bron[$sh["id"]] : $sh['seatsFree'],
                        "price" => $sh['prices'][0]['price'],
                        "id" => $Quest['ID']
                    )
                );
            }

            if($arError){
                echo "<pre>";
                print_r ( $arError );
            }

            echo json_encode($line);
        }
    }
}

function getSections($IBLOCK_ID, $arrData) {

    $arLists = [];
    $arSelect = $arrData;
    $arFilter = ["IBLOCK_ID" => '15',  "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'SECTION_ID' =>  ''];
    if(CModule::IncludeModule('iblock')) {

        $arFilter = ["IBLOCK_ID" => $IBLOCK_ID,  "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'SECTION_ID' =>  ''];

        $dbListBlock =  CIBlockSection::GetList(Array(), $arFilter, false, Array("UF_STYLE_ICON"), $arSelect);

        while($dbListBlocks = $dbListBlock->GetNext()){

            $arLists[] = $dbListBlocks;
        }
    }


    return $arLists;
}

function priceСhange($price, $date) {
    if (PERCENT > 0) {
        if (PERCENT != 100) {
            if (in_array($date, $GLOBALS['ARDATE'])) {
                $price = round($price - ($price / 100 * PERCENT));
            }
        }
    }

    return $price;
}

function priceColor($date) {
    $check = false;
    if (in_array($date, $GLOBALS['ARDATE'])) {
        $check = true;
    }

    return $check;
}

function post_content_CRM($token, $userData){
    #Формируем параметры запроса, описываем параметры пользователя которого хотим добавить
    $userdAdd = array('isVisitor' =>'false',
        'firstName' => $userData['name'],
        'lastName' => '',
        'about'=> $userData['phone'] . ' email- ' . $userData['email'],
    );

    #Формируем ссылку для запроса
    $linkPeople='URL_API';
    //api/2.0/crm/contact/person

    $headr = array('Content-type: application/json', 'Authorization: ' . $token);

    $curl = curl_init();
    curl_setopt($curl,CURLOPT_URL, $linkPeople);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($userdAdd));
    curl_setopt($curl,CURLOPT_HTTPHEADER, $headr);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $info = curl_getinfo($curl,CURLINFO_HTTP_CODE);

    $content =  curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
    curl_close($curl);
}


function post_content_token ($url, $postdata) {
    $uagent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)";

    $ch = curl_init( $url );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, "application/json");
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postdata));

    $content = curl_exec( $ch );
    curl_close( $ch );

    return $content;
}

function logerError($CURLINFO_CONTENT_TYPE, $status, $CURLINFO_APPCONNECT_TIME, $response_body, $errmsg) {

    global $DB;
    $DB->Query("INSERT INTO schedule_log  VALUES('0', '" . $CURLINFO_CONTENT_TYPE . "', '".$status."','".$CURLINFO_APPCONNECT_TIME."','". time() ."', '".$response_body."', '".$errmsg."') ");
}

function getGameIdByCode($game_id) {
    CModule::IncludeModule('iblock');

    $res = CIBlockElement::GetList(Array(),
        [
            "IBLOCK_ID" => QUESTS,
            "CODE" => $game_id,
            "ACTIVE_DATE" => "Y"
        ], false, false, [
            "ID", "NAME",
        ]);

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $id = $arFields['ID'];
    }

    return $id;
}

function checkData($id) {
    global $DB;
    $req = $DB->Query("SELECT seatsAll,seatsFree FROM schedule_two_weeks WHERE show_id = '" . $id . "'");
    if ($ob = $req->Fetch()) {

    }

    return $ob;
}

function checkReferer(){

    $HTTP_REFERER = getenv("HTTP_REFERER");

    if(stristr($HTTP_REFERER, $_SERVER['SERVER_NAME']) === FALSE) {
        $_SESSION['REFERER'] = $HTTP_REFERER;
        setcookie ("REFERER", $_SESSION['REFERER'], time()+3600);
    }
}

function dateNow() {

    $date_dashed = date("Y-m-d");
    $date = strtotime($date_dashed);
    $date = date("l", $date);
    $date = strtolower($date);
    $time_now = date('H:m');
    $checkPrice = 'false';
    
    if ($time_now >= '16:00' && $date == 'friday' ) {
        $checkPrice = 'true';
    }

    if ($date == 'saturday' || $date == 'sunday') {
        $checkPrice = 'true';
    }
    
    return $checkPrice;
}


function checkPriceDay() {
    $date_dashed = date("Y-m-d");
    $date = strtotime($date_dashed);
    $date = date("l", $date);
    $date = strtolower($date);
    $time_now = date('H:m');
    $checkPrice = 'false';
    
    if ($date == 'friday' ) {
        $checkPrice = 'true';
        $date_check = $date;
    }
    
    if ($date == 'saturday' || $date == 'sunday') {
        $checkPrice = 'true';
        $date_check = $date;
    }
    
    return ['day' => $checkPrice, 'data_type' => $date_check];
}

function dateAfter($date_time_current) {

    $date_dashed = date("Y-m-d", $date_time_current);
    $date = strtotime($date_dashed);

    $date = date("l", $date);

    $date = strtolower($date);
    $time_now = date('H:m', $date_time_current);
    $checkPrice = 'false';

    if ($time_now >= '16:00' && $date == 'friday' ) {/
        $checkPrice = 'true';
    }

    if ($date == 'saturday' || $date == 'sunday') {
        $checkPrice = 'true';
    }

     return $checkPrice;
}


function logerErrorTiket($url, $data, $out) {
    global $DB;
    $DB->Query("INSERT INTO tickets_log  VALUES('0', '" . $url . "', '" . $data . "','" . date('l jS \of F Y h:i:s A'). "','" . $out . "')");
}