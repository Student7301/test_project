<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

    <main class="lunch">
        <div class="lunch-wrapper">
            <?$APPLICATION->IncludeComponent("bitrix:news.list", 
                "business_lunch_list", 
                    Array(
                        "IBLOCK_ID" => "27",    // Код информационного блока
                        "PARENT_SECTION" => 174,
                        "FIELD_CODE" => array(  // Поля
                            0 => "DETAIL_TEXT",
                            1 => "DETAIL_PICTURE",
                        ),
                        "PROPERTY_CODE" => "",  // Свойства
                    ),
                false
            );?>

            <div class="lunch-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <?$APPLICATION->IncludeComponent("bitrix:news.list",
                                "lunch_menu_header", 
                                Array(
                                    "IBLOCK_ID" => "27",    // Код информационного блока
                                    "PARENT_SECTION" => 175,
                                    "FIELD_CODE" => array(  // Поля
                                        0 => "DETAIL_TEXT",
                                    ),
                                    "PROPERTY_CODE" => [
                                            0 => 'PRICE'
                                    ],  // Свойства
                                ),
                                false
                            );?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lunch-download">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="lunch-link--wrapper">
                                <a href="/upload/biznes_lanch.pdf" class="lunch-link">Скачать меню (pdf)</a>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>