<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Bootstrap;
use Tygh\Registry;
use Tygh\Storage;
use Tygh\Tools\Url;

use Tygh\Languages\Languages;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_sd_xml_import_list($params, $lang_code = CART_LANGUAGE)
{
    $default_params = array(
        'items_per_page' => '',
    );

    $params = array_merge($default_params, $params);

    $condition = $limit = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $condition = (AREA == 'A') ? '' : " AND ?:import_profiles.status = 'A' ";

    if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:import_profiles.import_profiles_id IN (?n)', explode(',', $params['item_ids']));
    }

    $fields = array (
        '?:import_profiles.import_profiles_id',
        '?:import_profiles.*',
        '?:import_profiles_descriptions.*'
    );

    $joins[] = db_quote(' LEFT JOIN ?:import_profiles_descriptions ON ?:import_profiles_descriptions.import_profiles_id = ?:import_profiles.import_profiles_id AND ?:import_profiles_descriptions.lang_code = ?s', $lang_code);

    if (!empty($params['import_id'])) {
        $condition = db_quote(' AND ?:import_profiles.import_profiles_id = ?i', $params['import_id']);
    }

    $condition .= fn_get_company_condition('?:import_profiles.company_id');

    $import_profiles = db_get_hash_array(
        'SELECT ?p FROM ?:import_profiles ' .
        ' LEFT JOIN ?:import_profiles_descriptions ON ?:import_profiles_descriptions.import_profiles_id = ?:import_profiles.import_profiles_id AND ?:import_profiles_descriptions.lang_code = ?s' .
        'WHERE 1 ?p ?p', 'import_profiles_id', implode(', ', $fields), $lang_code, $condition, $limit
    );

    return array($import_profiles, $params);
}

function fn_xml_imports_get_features_fields()
{
    $features = db_get_array('SELECT ?:product_features_descriptions.feature_id, ?:product_features_descriptions.description FROM ?:product_features_descriptions LEFT JOIN ?:product_features ON (?:product_features_descriptions.feature_id = ?:product_features.feature_id) WHERE ?:product_features.feature_type <> ?s AND lang_code = ?s', 'G', DESCR_SL);

    $features_fields = array();

    if (!empty($features)) {
        foreach ($features as $feature) {
            $features_fields[$feature['description']] = array(
                'process_get' => array ('fn_xml_imports_data_feeds_get_product_features', '#key', '#lang_code'),
                'linked' => false,
            );
        }
    }

    return $features_fields;
}

function fn_xml_imports_get_options_fields()
{
    $options_fields = db_get_hash_single_array('SELECT od.option_id, od.option_name FROM ?:product_options o INNER JOIN ?:product_options_descriptions od ON od.option_id = o.option_id WHERE o.product_id = ?i AND od.lang_code = ?s', array('option_id', 'option_name'), 0, DESCR_SL);

    return $options_fields;
}

function fn_get_xml_import_data($params = array(), $lang_code = CART_LANGUAGE)
{

    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:import_profiles.import_profiles_id',
        '?:import_profiles.*',
        '?:import_profiles_descriptions.*'
    );

    $joins[] = db_quote(' LEFT JOIN ?:import_profiles_descriptions ON ?:import_profiles_descriptions.import_profiles_id = ?:import_profiles.import_profiles_id AND ?:import_profiles_descriptions.lang_code = ?s', $lang_code);

    $condition = db_quote(' WHERE ?:import_profiles.import_profiles_id = ?i', $params['import_profiles_id']);
    $condition .= (AREA == 'A') ? '' : " AND ?:import_profiles.status IN ('A', 'H') ";

    $import_profiles = db_get_array(
        'SELECT ' . implode(', ', $fields) . ' FROM ?:import_profiles ' . implode(' ', $joins) ." $condition"
    );

    if (!empty($import_profiles)) {
        foreach ($import_profiles as &$import) {
            $import['fields'] = unserialize($import['fields']);
            $import['fields_mapping'] = unserialize($import['fields_mapping']);
            $import['export_options'] = unserialize($import['export_options']);
            $import['languages'] = unserialize($import['languages']);
        }
    }

    if (!empty($params['single']) && !empty($import_profiles)) {
        $import_profiles = array_pop($import_profiles);
        if (!empty($import_profiles['notifications']) && !empty($import_profiles['import_profiles_id'])) {
            $users = db_get_field('SELECT users FROM ?:import_user_notifications WHERE import_profile_id = ?i', $import_profiles['import_profiles_id']);
            $user_id = $_SESSION['auth']['user_id'];
            if (!empty($users)) {
                $users = json_decode($users, true);
            }
            if (empty($users[$user_id])) {
                $notifications = json_decode($import_profiles['notifications'], true);
                $_SESSION['notifications'] = array_merge($_SESSION['notifications'], $notifications);
                $users[$user_id] = $user_id;
                $users = json_encode($users);
                $data = array('import_profile_id' => $import_profiles['import_profiles_id'], 'users' => $users);
                db_query('REPLACE INTO ?:import_user_notifications ?e', $data);
            }
        }
    }

    return $import_profiles;
}

function fn_xml_imports_update($import_data, $import_id, $name_file, $file_url, $type_file, $lang_code = DESCR_SL)
{
    $reader = new XMLReader();
    $type_file = array_pop($type_file);
    $file_url = array_pop($file_url);
    if (!empty($file_url)) {
        if ($type_file == 'local') {
            $file = array_pop($name_file);
            fn_mkdir(Registry::get('config.dir.var') . $import_data['export_options']['files_path']);
            $file_name = TIME . '_' . $file['name'];
            fn_copy($file['path'], Registry::get('config.dir.var') . $import_data['export_options']['files_path'] . $file_name);
            $file_xml = Registry::get('config.dir.var') . $import_data['export_options']['files_path'] . '/'. $file_name;
            $import_data['url'] = $file_name;
        } elseif ($type_file== 'url') {
            $file_xml = $file_url;
            $import_data['url'] = $file_url;
        } else {
            $file_xml = array_pop($name_file);
            $file_xml = Registry::get('config.dir.var') . $import_data['export_options']['files_path'] . $import_data['url'];
        }
    } else {
        $data_file = parse_url($import_data['url'], 0);
        if ($data_file == 'https' or $data_file == 'http') {
            $file_xml = $import_data['url'];
        } else {
            $file_xml = Registry::get('config.dir.var') . $import_data['export_options']['files_path'] . $import_data['url'];
        }
    }
    $is_load_xml = true;
    if ($import_data['file_type'] == 'xml' && preg_match('/^(http[s]?)|(ftp[s]?):\/\//i', $file_xml)) {
        $file_headers = get_headers($file_xml);

        if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            fn_set_notification('E', __('notice'), __('sd_could_not_download_file'));
            $is_load_xml = false;
        }
    }

    if ($import_data['file_type'] == 'csv') {
        if (preg_match('/^(http[s]?)|(ftp[s]?):\/\//i', $file_xml)) {
            $file_xml = fn_xml_imports_get_tmp_csv_file($file_xml);
        }
        $data = fn_sd_xml_import_get_csv($file_xml, $import_data['export_options']);
        if (!empty($data)) {
            $data_xml = array_keys($data[0]);
        } else {
            $is_load_xml = false;
        } 
    } else {
        if ($is_load_xml) {
            libxml_use_internal_errors(true);
            $res = $reader->open($file_xml);
            $reader->setParserProperty(XMLReader::VALIDATE, true);
            if ($reader->isValid()) {
                $reader->setParserProperty(XMLReader::VALIDATE, false);
                while ($reader->read()) {
                    if ($reader->nodeType == XMLReader::ELEMENT) {
                        $xml = simplexml_load_string($reader->readOuterXML());
                        foreach ($xml as $key => $xml_data) {
                            foreach ($xml_data as $key => $value) {
                                $data_xml[] = $key;
                            }
                        }
                    }
                }
                if (count(libxml_get_errors())) {
                    fn_set_notification('E', __('notice'), __('sd_could_not_download_file'));
                    $is_load_xml = false;
                }
            } else {
                fn_set_notification('E', __('notice'), __('sd_could_not_download_file'));
                $is_load_xml = false;
            }
        }
    }

    if ($is_load_xml) {
        $import_data['lang_code'] = empty($import_data['lang_code']) ? array(CART_LANGUAGE) : $import_data['lang_code'];
        $import_data['languages'] = serialize($import_data['lang_code']);
        $import_data['fields_mapping'] = serialize(array_unique($data_xml));

        if (!empty($import_data['fields'])) {
            $fields = array();
            foreach ($import_data['fields'] as $field_id => $fields_data) {
                if (!empty($fields_data['export_field_name'])) {
                    $fields[] = $fields_data;
                    if ($fields_data['field'] == 'Category') {
                        fn_sd_xml_import_link_categories($import_data, $import_id, $lang_code);
                    }
                } elseif (empty($fields_data['export_field_name']) && $fields_data['field'] == 'product') {
                    fn_set_notification('E', __('notice'), __('product_name_obligatory'));
                    return $import_id;
                }
            }

            $import_data['fields'] = serialize($fields);
        }
        $import_data['export_options'] = serialize(!empty($import_data['export_options']) ? $import_data['export_options'] : array());

        if (fn_allowed_for('MULTIVENDOR') && Registry::get('runtime.company_id')) {
            $import_data['company_id'] = Registry::get('runtime.company_id');
        }

        if (empty($import_id)) {
            $import_id = db_query('INSERT INTO ?:import_profiles ?e', $import_data);
            if (!empty($import_id)) {
                $_data = array();
                $_data['import_profiles_id'] = $import_id;
                $_data['datafeed_name'] = $import_data['datafeed_name'];

                foreach (fn_get_translation_languages() as $_data['lang_code'] => $_v) {
                    db_query('INSERT INTO ?:import_profiles_descriptions ?e', $_data);
                }
            }

        } else {
            db_query('UPDATE ?:import_profiles SET ?u WHERE import_profiles_id = ?i', $import_data, $import_id);
            unset($import_data['lang_code']);
            db_query('UPDATE ?:import_profiles_descriptions SET ?u WHERE import_profiles_id = ?i AND lang_code = ?s', $import_data, $import_id, $lang_code);
        }
    } else {
        $import_id = 0;
    }

    return $import_id;

}

function fn_xml_imports_delete($import_profiles_ids)
{
    $import_profiles = db_get_array('SELECT url, export_options FROM ?:import_profiles  WHERE import_profiles_id IN (?a)', $import_profiles_ids);
    if (!empty($import_profiles)) {
        $var_path = Registry::get('config.dir.var');
        foreach ($import_profiles as $val) {
            $export_options = unserialize($val['export_options']);
            if (!preg_match('/^http[s]?:\/\//i', $val['url']) && file_exists($var_path . $export_options['files_path'] . $val['url'])) {
                unlink($var_path . $export_options['files_path'] . $val['url']);
            }
        }
    }
    db_query('DELETE FROM ?:import_profiles WHERE import_profiles_id IN (?a)', $import_profiles_ids);
    db_query('DELETE FROM ?:import_profiles_descriptions WHERE import_profiles_id IN (?a)', $import_profiles_ids);
}


function fn_exim_xml_prepare_data($value, $xml_to_cscart)
{
    $features_fields = fn_xml_imports_get_features_fields();
    $features = array_keys($features_fields);
    $options = fn_xml_imports_get_options_fields();
    $data = array();
    foreach ($value as $k => $v) {
        if ($v->count()) {
            $array = fn_exim_xml_prepare_data($v, $xml_to_cscart);
            $data = array_merge_recursive($data, $array);
        } elseif(!empty($xml_to_cscart[$k])) {
            foreach ($xml_to_cscart[$k] as $field) {
                if ($field['avail'] == 'Y') {
                    $data[$field['field']] = (string) $v;
                    if (in_array($field['field'], $features) && !empty($data[$field['field']])) {
                        $data['features'][$field['field']] = $data[$field['field']];
                    } elseif ($field['field'] == 'Additional images' && !empty($data[$field['field']])) {
                        $data['additional_images'][] = $data[$field['field']];
                    } elseif (in_array($field['field'], $options) && !empty($data[$field['field']])) {
                        $data['option_variants'][$field['field']][] = $data[$field['field']];
                    }
                }
            }
        }
        
    }
    return $data;
}

function fn_exim_xml_imports($import_data, $import_id, $lang_code = CART_LANGUAGE) 
{
    if ($import_data == 'D' && AREA == 'A') {
        return $import_id;
    }

    if (!empty($import_id)) {
        db_query('UPDATE ?:import_profiles SET cron_status = ?s WHERE import_profiles_id = ?i', 'xml_in_progress', $import_id);
    }

    $processed_data = array (
        'E' => 0, // existent
        'N' => 0, // new
        'S' => 0, // skipped
    );

    $reader = new XMLReader();

    if (preg_match('/^http[s]?:\/\//i', $import_data['url'])) {
        $file_xml = $import_data['url'];
    } else {
        $file_xml = Registry::get('config.dir.var') . $import_data['export_options']['files_path'] . $import_data['url'];
    }

    $reader->open($file_xml);
    $progress = 0;
    $features_fields = fn_xml_imports_get_features_fields();
    $features = array_keys($features_fields);
    $options = fn_xml_imports_get_options_fields();
    $is_empty_category = false;
    $is_empty_product_name = false;
    $not_linked_category = array();
    $not_exist_imgs = array();
    $not_exist_currensy = array();
    $xml_to_cscart = array();
    foreach ($import_data['fields'] as $field) {
        if ($field['export_field_name']) {
            $xml_to_cscart[$field['export_field_name']][] = array('field' => $field['field'], 'avail' => $field['avail']);
        }
    }

    $xmldata = array();
    if ($import_data['file_type'] == 'csv') {
        if (preg_match('/^(http[s]?)|(ftp[s]?):\/\//i', $file_xml)) {
            $file_xml = fn_xml_imports_get_tmp_csv_file($file_xml);
        }
        $data = fn_sd_xml_import_get_csv($file_xml, $import_data['export_options']);
        if (!empty($data)) {
            $i = 0;
            foreach ($data as $value) {
                foreach ($xml_to_cscart as $key => $fields) {
                    foreach ($fields as $field) {
                        if (!empty($value[$key])) {
                            foreach ($value[$key] as $val) {
                               if ($field['avail'] == 'Y') {
                                    $xmldata[$i][$field['field']] = $val;
                                }
                                if (in_array($field['field'], $features) && !empty($xmldata[$i][$field['field']])) {
                                    $xmldata[$i]['features'][$field['field']] = $xmldata[$i][$field['field']];
                                } elseif ($field['field'] == 'Additional images' && !empty($xmldata[$i][$field['field']])) {
                                    $xmldata[$i]['additional_images'][] = $xmldata[$i][$field['field']];
                                } elseif (in_array($field['field'], $options) && !empty($xmldata[$i][$field['field']])) {
                                    $xmldata[$i]['option_variants'][$field['field']][] = $xmldata[$i][$field['field']];
                                }
                            }
                        }
                    }
                }
                $i++;
            }
        }
    } else {
        while ($reader->read()) {
            if ($reader->nodeType == XMLReader::ELEMENT) {
                $xml = simplexml_load_string($reader->readOuterXML());
                if ($xml->count() < 2) {
                    continue;
                }

                $i = 0;
                foreach ($xml as $key => $value) {
                    $xmldata[$i] = fn_exim_xml_prepare_data($value, $xml_to_cscart);
                    $i++;
                }
                break;
            }
        }
    }
    $cache_currency = array();
    if (!empty($xmldata)) {
        $images_path = Registry::get('config.dir.var').$import_data['export_options']['images_path'];
        fn_set_progress('parts', sizeof($xmldata));
        foreach ($xmldata as $data) {
            $is_exist_product = false;
            if (empty($import_data['company_id'])) {
                $company_id = Registry::ifGet('runtime.company_id', fn_get_default_company_id());;
            } else {
                $company_id = $import_data['company_id'];
            }

            $data['company_id'] = $company_id;

            if (empty($data['product'])) {
                $processed_data['S']++;
                $is_empty_product_name = true;
                continue;
            }
            $data['product'] = trim($data['product']);
            $product_id = db_get_field('SELECT MAX(p.product_id) FROM ?:products p INNER JOIN ?:product_descriptions pd on pd.product_id = p.product_id WHERE p.company_id = ?i AND pd.product = ?s AND pd.lang_code = ?s', $company_id, $data['product'], $lang_code);

            if (empty($data['Category'])) {

                $is_exist_category = false;
                if (!empty($product_id)) {
                    $is_exist_category = db_get_field('SELECT category_id FROM ?:products_categories WHERE product_id = ?i', $product_id);
                } 

                if (!$is_exist_category) {
                    $processed_data['S']++;
                    $is_empty_category = true;
                    continue;
                }
            }

            if (!empty($data['tax_ids'])) {

                $tax_id = '';
                if (empty($import_data['prices'])) {
                    $import_data['prices'] = array(
                        'lower_limit' => '',
                        'type' => ''
                    );
                }

                $tax_data = array(
                    'tax' => $data['tax_ids'],
                    'status' => 'A',
                    'address_type' => 'S',
                    'price_includes_tax' => 'N',
                );

                $taxs_id = db_get_field('SELECT tax_id FROM ?:tax_descriptions WHERE tax = ?i', $data['tax_ids']);
                $rate_id = '';

                if (!empty($taxs_id)){
                    $rate_id = db_get_field('SELECT rate_id FROM ?:tax_rates WHERE tax_id = ?i', $tax_id);
                }

                $tax_data['rates'] = array(
                    '1' => array(
                        'rate_value' => $data['tax_ids'],
                        'rate_type' => 'P',
                        'tax_id' => $taxs_id,
                    ),
                );

                $tax_id = fn_update_tax($tax_data, $taxs_id, DESCR_SL);
                $data['tax_ids'] = array($tax_id);
            }

            if (!isset($data['price'])) {
                $data['price'] = 0;
            }

            if (!empty($data['price'])) {
                $data['price'] = str_replace($import_data['export_options']['price_dec_sign_delimiter'], '.', $data['price']);
            }

            if (!empty($data['list_price'])) {
                $price = explode($import_data['export_options']['price_dec_sign_delimiter'], $data['list_price']);
                $data['list_price'] = str_replace($import_data['export_options']['price_dec_sign_delimiter'], '.', $data['list_price']);
            }
            $data['Currency'] = !empty($data['Currency']) ? strtoupper($data['Currency']) : '';
            if (!empty($data['Currency']) && $data['Currency'] != CART_PRIMARY_CURRENCY) {
                $currencies = fn_get_currencies_list(array(), 'A', CART_LANGUAGE);
                if (!empty($currencies[$data['Currency']])) {
                    $coefficient = $currencies[$data['Currency']]['coefficient'] ? 1 / $currencies[$data['Currency']]['coefficient'] : 1;
                } else {
                    if (empty($cache_currency[$data['Currency']])) {
                        //get currensy from service
                        $currency_data = file_get_contents('https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+"'. CART_PRIMARY_CURRENCY . $data['Currency'] . '"&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys');

                        if (!empty($currency_data)) {
                            $json = json_decode($currency_data, true);
                            $cache_currency[$data['Currency']] = (!empty($json['query']['results']['rate']['Rate'])) ? (float) $json['query']['results']['rate']['Rate'] : null;
                        }
                    }
                    if (!empty($cache_currency[$data['Currency']])) {
                        $coefficient = $cache_currency[$data['Currency']];
                    }
                }

                if (!empty($coefficient)) {
                    if (!empty($data['price']))  {
                        $data['price'] /= $coefficient;
                    }
                    if (!empty($data['list_price']))  {
                        $data['list_price'] /= $coefficient;
                    }
                } else {
                    $not_exist_currensy[$data['Currency']] = $data['Currency'];
                }
            }

            if (!empty($import_data['price_margin'])) {
                if ($import_data['type_price_margin'] == 'F') {
                    $data['price'] = $data['price'] + strval($import_data['price_margin']);
                } else {
                    $price_margin = fn_format_price(floatval($import_data['price_margin']/100));
                    $price = fn_format_price(floatval($data['price']));
                    $data['price'] = fn_format_price(floatval($data['price'])) + ($price * $price_margin);
                }
            }

            if(!empty($product_id)) {
                $is_exist_product = true;
                $product_id = fn_update_product($data, $product_id);
            } else {
                $product_id = fn_update_product($data);
            }

            if (!$product_id) {
                $processed_data['S']++;
                continue;
            }

            if (!empty($data['Category'])) {

                $category_id = db_get_field('SELECT category_id FROM ?:import_category_links WHERE import_profiles_id = ?i AND category = ?s', $import_id, $data['Category']);
                if (empty($category_id)) {
                    $processed_data['S']++;
                    $not_linked_category[$data['Category']] = $data['Category'];
                    continue;
                }

                $data_categories = array(
                    'product_id' => $product_id,
                    'category_id' => $category_id,
                    'link_type' => 'M'
                );

                db_query('DELETE FROM ?:products_categories WHERE product_id = ?i AND category_id NOT IN (?a)', $data_categories['product_id'], $data_categories['category_id']);

                if (!empty($product_id)){
                    db_query('REPLACE INTO ?:products_categories ?e', $data_categories);
                }

                $updated_categories[] = $category_id;
                if (!empty($updated_categories)) {
                    fn_update_product_count($updated_categories);
                }
            }

            if (empty($data['product'])) {
                $processed_data['S']++;
            } else {
                if (!$is_exist_product) {
                    $processed_data['N']++;
                } else {
                    $processed_data['E']++;
                }
            }

            if (empty($import_data['cron'])) {
                $count_langs = 1;
                $progress += $count_langs;

                fn_set_progress('echo', __('importing_data') . ':&nbsp;<b>' . ($progress) .'</b>', true);
            }
            if (!empty($data['features'])) {
                foreach ($data['features'] as $key => $val) {
                    $feature_id = db_get_field('SELECT feature_id FROM ?:product_features_descriptions WHERE description = ?s AND lang_code = ?s', $key, $lang_code);
                    $variant_id = db_get_field('SELECT variant_id FROM ?:product_feature_variant_descriptions WHERE variant = ?s AND lang_code = ?s', $val, $lang_code);
                    db_query('DELETE FROM ?:product_features_values WHERE product_id = ?i AND feature_id = ?i', $product_id, $feature_id);
                    $variant = array(
                        'feature_id' => $feature_id ? $feature_id : '',
                        'description' => $val,
                        'variant' => $val,
                        'variant_id' => $variant_id ? $variant_id : '',
                        'product_id' => $product_id,
                        'lang_code' => $lang_code
                    );
                    $features_values_id = fn_update_product_feature_variant($feature_id, 'E', $variant);
                    db_query('REPLACE INTO ?:product_features_values ?e', $variant);
                }
            }

            if (!empty($data['option_variants'])) {
                foreach ($data['option_variants'] as $key => $val) {
                    $option_id = array_search($key, $options);
                    if ($option_id !== false) {
                        if ($product_id) {
                            db_query("REPLACE INTO ?:product_global_option_links (option_id, product_id) VALUES(?i, ?i)", $option_id , $product_id);    
                        }
                        foreach ($val as $option_variant) {
                            $variant_id = db_get_field('SELECT ov.variant_id FROM ?:product_option_variants ov INNER JOIN ?:product_option_variants_descriptions vd ON vd.variant_id = ov.variant_id WHERE ov.option_id = ?i AND vd.variant_name = ?s', $option_id, $option_variant);
                            if (!$variant_id) {
                                $option_variants_data = array('option_id' => $option_id);
                                $variant_id = db_query('INSERT INTO ?:product_option_variants ?e', $option_variants_data);
                                if ($variant_id) {
                                    foreach (fn_get_translation_languages() as $option_lang_code => $_v) {
                                        $option_variants_descriptions = array('variant_id' => $variant_id, 'lang_code' => $option_lang_code, 'variant_name' => $option_variant);
                                        db_query('REPLACE INTO ?:product_option_variants_descriptions ?e', $option_variants_descriptions);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($data['detailed_id'])) {
                if (!preg_match('/^(http[s]?)|(ftp[s]?):\/\//i', $data['detailed_id'])) {
                    if (file_exists($images_path.$data['detailed_id'])) {
                        $data['detailed_id'] = $images_path.$data['detailed_id'];
                    }
                }

                $img = fn_get_contents($data['detailed_id']);
                if (strlen($img)) {
                    $tmp = fn_create_temp_file();
                    $file = fn_put_contents($tmp, $img);
                    
                    $date_image = getimagesize($data['detailed_id']);

                    $detailed = array(
                        array(
                            'path' => $tmp,
                            'name' => fn_basename($data['detailed_id']),
                            'type' => $date_image['mime'],
                            'size' => $date_image['bits'],
                        ),
                    );

                    $pairs_data = array(
                        array(
                            'path' => $tmp,
                            'name' => fn_basename($data['detailed_id']),
                            'type' => 'M',
                        ),
                    );

                    fn_update_image_pairs('', $detailed, $pairs_data, $product_id, 'product', '', true);
                } else {
                    $not_exist_imgs[$data['detailed_id']] = $data['detailed_id'];
                }
            }

            if (!empty($data['additional_images'])) {
                foreach ($data['additional_images'] as $key => $value) {
                    if (!preg_match('/^(http[s]?)|(ftp[s]?):\/\//i', $value)) {
                        if (file_exists($images_path.$value)) {
                            $value = $images_path.$value;
                        }
                    }

                    $img = fn_get_contents($value);
                    if (strlen($img)) {
                        $tmp = fn_create_temp_file();
                        $file = fn_put_contents($tmp, $img);
                        
                        $date_image = getimagesize($value);

                        $detailed = array(
                            array(
                                'path' => $tmp,
                                'name' => fn_basename($value),
                                'type' => $date_image['mime'],
                                'size' => $date_image['bits'],
                            ),
                        );

                        $pairs_data = array(
                            array(
                                'path' => $tmp,
                                'name' => fn_basename($value),
                                'type' => 'A',
                            ),
                        );

                        $image_name =  '%' . $detailed[0]['name'] . '%';
                        $image_id = db_get_field('SELECT i.image_id FROM ?:images i INNER JOIN ?:images_links il ON il.detailed_id = i.image_id WHERE i.image_path like ?l AND il.object_id = ?i AND il.object_type = ?s', $image_name, $product_id, 'product');
                        if (empty($image_id)) {
                            fn_update_image_pairs('', $detailed, $pairs_data, $product_id, 'product', '', true);
                        }
                    } else {
                        $not_exist_imgs[$value] = $value;
                    }
                }
            }
        }
    }

    if (!empty($not_exist_imgs)) {
        fn_set_notification('E', __('notice'), __('not_exist_imgs') . ': ' . implode(', ', $not_exist_imgs));
    }
    if (!empty($not_exist_currensy)) {
        fn_set_notification('E', __('notice'), __('not_exist_currensy') . ': ' . implode(', ', $not_exist_currensy));
    }
    if ($is_empty_product_name) {
        fn_set_notification('E', __('notice'), __('product_name_is_required'));
    }
    if ($is_empty_category) {
        fn_set_notification('E', __('notice'), __('not_empty_category'));
    }
    if (!empty($not_linked_category)) {
        fn_set_notification('E', __('notice'), __('not_linked') . ': ' . implode(', ', $not_linked_category));
    }

    fn_set_notification('W', __('important'), __('text_exim_data_imported', array(
        '[new]' => $processed_data['N'],
        '[exist]' => $processed_data['E'],
        '[skipped]' => $processed_data['S'],
        '[total]' => $processed_data['E'] + $processed_data['N'] + $processed_data['S']
    )));

    $cron_status = 'xml_complete';
    $notifications = null;
    if (!empty($_SESSION['notifications'])) {
        foreach ($_SESSION['notifications'] as $val) {
            if ($val['type'] == 'E') {
                $cron_status = 'xml_failed';
                break;
            }
        }
        if (!empty($import_data['cron'])) {
            $notifications = json_encode($_SESSION['notifications']);
            unset($_SESSION['notifications']);
        }
    }

    if (!empty($import_id)) {
        $data = array('cron_status' => $cron_status, 'finish_time' => TIME, 'notifications' => $notifications);
        db_query('UPDATE ?:import_profiles SET ?u WHERE import_profiles_id = ?i', $data, $import_id);
        db_query('DELETE FROM ?:import_user_notifications WHERE import_profile_id = ?i', $import_id);
    }

    return $import_id;
}

function fn_sd_xml_import_info_url($import_id = 0)
{
    $url = fn_url('run_import_xml.import');
    $text = __('export_cron_hint') . '<br/ >' . 'php /path/to/cart/'.fn_url('', 'A', 'rel').'  --dispatch=run_import_xml.import --cron_password=' . Registry::get('addons.sd_xml_import.cron_password');
    if ($import_id) {
        $text .= ' --import_id=' . $import_id;
    }

    return $text;
}

function fn_sd_xml_import_link_categories($import_data, $import_profiles_id, $lang_code = CART_LANGUAGE)
{
    $reader = new XMLReader();

    if (preg_match('/^http[s]?:\/\//i', $import_data['url'])) {
        $file_xml = $import_data['url'];
    } else {
        $file_xml = Registry::get('config.dir.var') . $import_data['export_options']['files_path'] . $import_data['url'];
    }
    $exist_category = db_get_hash_array('SELECT category_id, category FROM ?:import_category_links WHERE import_profiles_id = ?i', 'category', $import_profiles_id);

    $time_add_group = TIME;
    $new_category = array();
    $categories = array();

    if ($import_data['file_type'] == 'xml') {
        $reader->open($file_xml);
        while ($reader->read()) {
            if ($reader->nodeType == XMLReader::ELEMENT) {
                $xml = simplexml_load_string($reader->readOuterXML());
                foreach ($xml as $key => $value) {
                    $category = '';
                    foreach ($import_data['fields'] as $key => $field) {
                        if (!empty($field['export_field_name']) && $field['avail'] == 'Y' && $field['field'] == 'Category'){
                            $category = strval((string) $value->{$field['export_field_name']});
                            break;
                        }
                    }
                    if (!empty($category)) {
                        $categories[] = $category;
                    }
                }
            }
        }
    } else {
        if (preg_match('/^(http[s]?)|(ftp[s]?):\/\//i', $file_xml)) {
            $file_xml = fn_xml_imports_get_tmp_csv_file($file_xml);
        }
        $data = fn_sd_xml_import_get_csv($file_xml, $import_data['export_options']);
        if (!empty($data)) {
            foreach ($data as $value) {
                $category = '';
                foreach ($import_data['fields'] as $key => $field) {
                    if (!empty($field['export_field_name']) && $field['avail'] == 'Y' && $field['field'] == 'Category'){
                        $category = !empty($value[$field['export_field_name']][0]) ? $value[$field['export_field_name']][0] : '';
                        break;
                    }
                }
                if (!empty($category)) {
                    $categories[] = $category;
                }
            }
        } 
    }

    if (!empty($categories)) {
        foreach ($categories as $category) {
            $categories_ids = explode($import_data['export_options']['category_delimiter'], $category);
            $categories_ids = array_diff($categories_ids, array(''));
            $parent_id = '';
            foreach ($categories_ids as $cat) {
                $main_cat = $cat;
                $category_id = db_get_field('SELECT ?:categories.category_id FROM ?:category_descriptions INNER JOIN ?:categories ON ?:categories.category_id = ?:category_descriptions.category_id WHERE ?:category_descriptions.category = ?s AND lang_code = ?s AND parent_id = ?i', $main_cat, $lang_code, $parent_id);

                if (!empty($category_id)) {
                    $parent_id = $category_id;
                } else {
                    break;
                }
            }
            if (empty($exist_category[$category])) {
                $category_id = $category_id ? $category_id : null;
                $data = array('import_profiles_id' => $import_profiles_id, 'category' => $category, 'category_id' => $category_id, 'time_add_group' => $time_add_group);
                db_query('INSERT INTO ?:import_category_links ?e', $data);
                $exist_category[$category] = $data;
            }
            $new_category[] = $category;
        }
    }

    $diff_categories = array_diff(array_keys($exist_category), $new_category);
    if (!empty($diff_categories)) {
        db_query('DELETE FROM ?:import_category_links WHERE import_profiles_id = ?i AND category IN(?a)', $import_profiles_id, $diff_categories);
    }

    return true;
}

function fn_sd_xml_import_get_categories($import_profiles_id, $category_link_id = null, $lang_code = CART_LANGUAGE)
{
    if (!empty($import_profiles_id)) {
        $condition = '';
        if ($category_link_id) {
            $condition = db_quote(' AND cl.category_link_id = ?i', $category_link_id);
        }

        $order = ' ORDER BY time_add_group DESC, category_link_id';
                $categories = db_get_hash_multi_array('SELECT DISTINCT cl.*, c.category_id, c.id_path, CASE WHEN c.category_id > 0 THEN ?s ELSE ?s END AS is_linked FROM ?:import_category_links cl LEFT JOIN ?:categories c ON c.category_id = cl.category_id WHERE import_profiles_id = ?i' . $condition . $order, array('is_linked'), 'Y', 'N', $import_profiles_id);
        $path_names = array();

        foreach (array('N', 'Y') as $is_linked) {
            if (!empty($categories[$is_linked])) {
                foreach ($categories[$is_linked] as $key => $val) {
                    if (!empty($path_names[$val['id_path']])) {
                        $ancestors = $path_names[$val['id_path']];
                    } else {
                       $path = explode('/', $val['id_path']);
                        if ($path) {
                            $ancestors = db_get_array(
                                "SELECT ?:categories.category_id, ?:category_descriptions.category"
                                . " FROM ?:categories"
                                . " LEFT JOIN ?:category_descriptions"
                                . " ON ?:category_descriptions.category_id = ?:categories.category_id"
                                . " AND ?:category_descriptions.lang_code = ?s"
                                . " WHERE ?:categories.category_id IN (?n)",
                                $lang_code,
                                $path
                            );
                            $ancestors = fn_array_column(fn_sort_by_ids($ancestors, $path, 'category_id'), 'category', 'category_id');
                            $path_names[$val['id_path']] = $ancestors;
                        }
                    }
                    $categories[$is_linked][$key]['path_names'] = $ancestors;
                }
            }
        }
    } else {
        $categories = array();
    }

    return $categories;
}

function fn_sd_xml_import_create_category($category_link_id, $parent_category_id, $lang_code = CART_LANGUAGE)
{
    $data = explode(',', $category_link_id);
    foreach ($data as $category_link_id) {
        $category = db_get_row('SELECT cl.category, ip.export_options, ip.company_id FROM ?:import_category_links cl INNER JOIN ?:import_profiles ip ON ip.import_profiles_id = cl.import_profiles_id WHERE cl.category_link_id = ?i', $category_link_id);

        $category_id = '';
        if (!empty($category)) {
            $company_id = $category['company_id'] ? $category['company_id'] : Registry::ifGet('runtime.company_id', fn_get_default_company_id());
            $category['export_options'] = unserialize($category['export_options']);
            $categories_ids = explode($category['export_options']['category_delimiter'], $category['category']);
            $categories_ids = array_diff($categories_ids, array(''));
            $parent_id = $parent_category_id ? $parent_category_id : '';
            foreach ($categories_ids as $cat) {
                $main_cat = $cat;
                $category_id = db_get_field('SELECT ?:categories.category_id FROM ?:category_descriptions INNER JOIN ?:categories ON ?:categories.category_id = ?:category_descriptions.category_id WHERE ?:category_descriptions.category = ?s AND lang_code = ?s AND parent_id = ?i', $main_cat, $lang_code, $parent_id);
                if (!empty($category_id)) {
                    $parent_id = $category_id;
                } else {
                    $category_data = array(
                        'parent_id' => $parent_id,
                        'category' => $main_cat,
                        'timestamp' => TIME,
                        'company_id' => $company_id
                    );

                    $category_id = fn_update_category($category_data);
                    $parent_id = $category_id;
                }
            }
        }
        if ($category_id) {
            db_query('UPDATE ?:import_category_links SET category_id = ?i, time_add_group = ?i WHERE category_link_id = ?i', $category_id, TIME, $category_link_id);
        }
    }
    return true;
}

function fn_sd_xml_import_link_category($category_link_id, $category_id)
{
    db_query('UPDATE ?:import_category_links SET category_id = ?i, is_manual = ?s, time_add_group = ?i WHERE category_link_id = ?i', $category_id, 'Y', TIME, $category_link_id);
    return $category_id;
}

function fn_xml_imports_get_tmp_csv_file($file_xml)
{
    $csv_file = '';
    if (!empty($file_xml)) {
        $tmp = file_get_contents($file_xml);
        $csv_file = fn_create_temp_file();
        fn_put_contents($csv_file, $tmp);
    }
    return $csv_file;
}

function fn_sd_xml_import_get_csv($file, $options)
{
    $max_line_size = 65536; // 64 Кб
    $result = array();

    if ($options['delimiter'] == 'C') {
        $delimiter = ',';
    } elseif ($options['delimiter'] == 'T') {
        $delimiter = "\t";
    } else {
        $delimiter = ';';
    }

    if (!empty($file) && file_exists($file)) {

        $encoding = fn_detect_encoding($file, 'F', !empty($options['lang_code']) ? $options['lang_code'] : CART_LANGUAGE);

        if (!empty($encoding)) {
             $file = fn_convert_encoding($encoding, 'UTF-8', $file, 'F');
        } else {
            fn_set_notification('W', __('warning'), __('text_exim_utf8_file_format'));
        }

        $f = false;
        if ($file !== false) {
            $f = fopen($file, 'rb');
        }

        if ($f) {
            // Get import schema
            $import_schema = fgetcsv($f, $max_line_size, $delimiter);
            if (empty($import_schema)) {
                fn_set_notification('E', __('error'), __('error_exim_cant_read_file'));

                return false;
            }

            // Collect data
            $schema_size = sizeof($import_schema);
            $skipped_lines = array();
            $line_it = 1;
            while (($data = fn_fgetcsv($f, $max_line_size, $delimiter)) !== false) {

                $line_it ++;
                if (fn_is_empty($data)) {
                    continue;
                }

                if (sizeof($data) != $schema_size) {
                    $skipped_lines[] = $line_it;
                    continue;
                }

                $data = Bootstrap::stripSlashes($data);
                $temp = array();
                if (!empty($import_schema)) {
                    foreach ($import_schema as $key => $value) {
                        $temp[$value][] = !empty($data[$key]) ? $data[$key] : '';
                    }
                }

                $result[] = $temp;
            }

            if (!empty($skipped_lines)) {
                fn_set_notification('W', __('warning'), __('error_exim_incorrect_lines', array(
                    '[lines]' => implode(', ', $skipped_lines)
                )));
            }

            return $result;
        } else {
            fn_set_notification('E', __('error'), __('error_exim_cant_open_file'));

            return false;
        }
    } else {
        fn_set_notification('E', __('error'), __('error_exim_file_doesnt_exist'));

        return false;
    }
}

function fn_sd_xml_import_check_user_type_access_rules_pre($user_data, $account_type, &$rules)
{
    if (Registry::get('addons.sd_xml_import.import_for_vendor') == 'Y' && Registry::get('runtime.controller') == 'run_import_xml') {
        $rules['A'][] = 'vendor';
    }
}