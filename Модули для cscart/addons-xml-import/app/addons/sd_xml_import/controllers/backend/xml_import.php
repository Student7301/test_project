<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Http;
use Tygh\Languages\Languages;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']  == 'POST') {

    if (defined('AJAX_REQUEST') && $mode == 'link_category') {

        if (!empty($_REQUEST['category_link_id'])) {
            if (isset($_REQUEST['category_id'])) {
                $category_id = fn_sd_xml_import_link_category($_REQUEST['category_link_id'], $_REQUEST['category_id']);
            }
            if (!empty($_REQUEST['import_profiles_id'])) {
                $categories = fn_sd_xml_import_get_categories($_REQUEST['import_profiles_id'], $_REQUEST['category_link_id']);
            }
        }
        $category = !empty($categories['Y'][0]) ? $categories['Y'][0] : '';
        Registry::get('view')->assign('category', $category);
        Registry::get('view')->display('addons/sd_xml_import/views/xml_import/components/category.tpl');
        exit;
    }

    if ($mode == 'create_category' && !empty($_REQUEST['category_link_id']) && isset($_REQUEST['category_id']) && !empty($_REQUEST['import_profiles_id'])) {
        $category_id = fn_sd_xml_import_create_category($_REQUEST['category_link_id'], $_REQUEST['category_id']);    
        $_REQUEST['selected_section'] = 'category_mapping';
        $suffix = ".update?selected_section=category_mapping&import_profiles_id=".$_REQUEST['import_profiles_id'];
    }

    if ($mode == 'm_delete') {

        fn_xml_imports_delete($_REQUEST['import_profiles_ids']);

        $suffix = '.manage';
    }

    if ($mode == 'run_import') {
        if ($_REQUEST['datafeed_data']['status'] == 'D') {
            fn_set_notification('E', __('notice'), __('sd_please_activate_status'));

            return;
        }
        if (!empty($_REQUEST['import_profiles_id'])) {
            $import_id = fn_exim_xml_imports($_REQUEST['datafeed_data'], $_REQUEST['import_profiles_id']);
        }

    $suffix = ".update?import_profiles_id=$import_id";

    }  elseif ($mode == 'delete') {

        if (!empty($_REQUEST['import_profiles_id'])) {
            fn_xml_imports_delete($_REQUEST['import_profiles_id']);
        }

        $suffix = '.manage';
    }

    if ($mode == 'update') {
        $xml_data_file = '';
        if (!empty($_REQUEST['type_file'][0]) && $_REQUEST['type_file'][0] != 'url') {
            $xml_data_file = fn_filter_uploaded_data('file');
        }
        if (empty($_REQUEST['file_file']['0']) && empty($_REQUEST['datafeed_data']['url'])) {
            fn_set_notification('E', __('notice'), __('select_file_to_upload'));
            $suffix = ".add";
        } else {
            $import_id = fn_xml_imports_update($_REQUEST['datafeed_data'], $_REQUEST['import_profiles_id'], $xml_data_file, $_REQUEST['file_file'], $_REQUEST['type_file'], DESCR_SL);
            if (!$import_id) {
                return array(CONTROLLER_STATUS_REDIRECT, 'xml_import.add');
            }
            $suffix = ".update?import_profiles_id=$import_id";
        }
        $exists_category = false;
        if (!empty($_REQUEST['datafeed_data']['fields'])) {
            foreach ($_REQUEST['datafeed_data']['fields'] as $val) {
                if ($val['field'] == 'Category' && $val['export_field_name']) {
                    $exists_category = true;
                    break;
                }
            }
        }
        if (!empty($_REQUEST['selected_section'])) {
            if ($_REQUEST['selected_section'] == 'general' && empty($_REQUEST['import_profiles_id'])) {
                $_REQUEST['selected_section'] = 'fields_mapping';
            } elseif ($_REQUEST['selected_section'] == 'fields_mapping' && $exists_category) {
                $_REQUEST['selected_section'] = 'category_mapping';
            }
        }
    }

    return array(CONTROLLER_STATUS_OK, 'xml_import' . $suffix);
}

if ($mode == 'manage') {

    list($datafeeds) = fn_sd_xml_import_list(array(), DESCR_SL);

    Registry::get('view')->assign('datafeeds', $datafeeds);

} elseif ($mode == 'add') {

    $pattern = fn_get_schema('exim', 'xml_import');
    Registry::get('view')->assign('pattern', $pattern);

    foreach (fn_get_translation_languages() as $lang_code => $lang_data) {
        $export_langs[$lang_code]['name'] = $lang_data['name'];
    }

    Registry::get('view')->assign('export_langs', $export_langs);
    Registry::get('view')->assign('export_fields', $pattern['export_fields']);
    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));

} elseif ($mode == 'update') {

    $params['import_profiles_id'] = $_REQUEST['import_profiles_id'];
    $params['single'] = true;
    $datafeed_data = fn_get_xml_import_data($params, DESCR_SL);
    Registry::get('view')->assign('datafeed_data', $datafeed_data);
    $tabs = array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
        'fields_mapping' => array(
            'title' => __('map_fields'),
            'js' => true
        ),
    );
    if (!empty($datafeed_data['fields'])) {
        foreach ($datafeed_data['fields'] as $fields) {
            if ($fields['field'] == 'Category') {
                $tabs['category_mapping'] = array ('title' => __('category_mapping'), 'js' => true);
                $categories = fn_sd_xml_import_get_categories($_REQUEST['import_profiles_id']);
                Registry::get('view')->assign('categories', $categories);
                break;
            }
        }
        $tabs['cron'] = array ('title' => __('cron'), 'js' => true);
    }
    
    foreach (fn_get_translation_languages() as $lang_code => $lang_data) {
        $export_langs[$lang_code]['name'] = $lang_data['name'];
        foreach ($datafeed_data['languages'] as $key => $lang_code_import) {
            if (!empty($lang_code_import) && $lang_code_import == $lang_code) {
                $export_langs[$lang_code]['status'] = 'A';
            }
        }
    }

    Registry::get('view')->assign('export_langs', $export_langs);
    $pattern = fn_get_schema('exim', 'xml_import');
    Registry::get('view')->assign('pattern', $pattern);
    Registry::get('view')->assign('export_fields', $pattern['export_fields']);
    $path_info = explode('.', $datafeed_data['url']);
    $path_info = end($path_info);
    Registry::get('view')->assign('path_info', $path_info);

    if (empty($datafeed_data)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    Registry::set('navigation.tabs', $tabs);

    Registry::get('view')->assign('options_fields', fn_xml_imports_get_options_fields());
    Registry::get('view')->assign('feature_fields', fn_xml_imports_get_features_fields());
}