{if "MULTIVENDOR"|fn_allowed_for}
    <div class="control-group setting-wide">
        <label class="control-label">{__("import_for_vendor")}:</label>
        <div class="controls">
            <input type="hidden" value="N" name="import_for_vendor_value"/>
            <input type="checkbox" value="Y" name="import_for_vendor_value" {if $addons.sd_xml_import.import_for_vendor == "Y"}checked{/if}/>
        </div>
    </div>
{/if}