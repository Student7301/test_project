{capture name="mainbox"}
<script type="text/javascript">
(function(_, $) {
    $(".button_save").click(function(){
        $(".form-edit").removeClass("cm-ajax cm-comet");
    });

    $(".cm-tab-tools").click(function(){
        $(".form-edit").addClass("cm-ajax cm-comet");
    });

    var original_val = $.fn.val;
    $.fn.val = function(){
        var result = original_val.apply(this, arguments);
        if (arguments.length > 0) {
            $(this).change();
        }
        return result;
    };


    $(document).ready(function(){
        _.fileuploader['show_loader'] = function(elm_id) {
            var suffix = elm_id.str_replace('local_', '').str_replace('server_', '').str_replace('url_', '');
            var max_file_size_bytes;
            var max_file_size_mbytes;
            var file_holder;
            var native_file_holder;

            if (elm_id.indexOf('local') != -1) {
                file_holder = $('#' + elm_id);
                native_file_holder = file_holder[0];
                this.display_filename(suffix, 'local', file_holder.val());

                // Check if file size more than available POST_MAX_SIZE
                if (native_file_holder.files && native_file_holder.files[0]) {
                    if (parseInt(_.post_max_size_bytes) == 0 || (parseInt(_.post_max_size_bytes) > parseInt(_.files_upload_max_size_bytes))) {
                        max_file_size_bytes = _.files_upload_max_size_bytes;
                        max_file_size_mbytes = _.files_upload_max_size_mbytes;
                    } else {
                        max_file_size_bytes = _.post_max_size_bytes;
                        max_file_size_mbytes = _.post_max_size_mbytes;
                    }

                    if (native_file_holder.files[0].size > max_file_size_bytes) {
                        $.ceNotification('show', {
                            type: 'E',
                            title: _.tr('error'),
                            message: _.tr('file_is_too_large').replace('[size]', max_file_size_mbytes)
                        });
                        this.clean_form();
                    } else if (parseInt(_.post_max_size_bytes)) {

                        var sum_files_size = 0;
                        $("[type=file]").each(function() {
                            if ($(this)[0].files.length > 0) {
                                sum_files_size += $(this)[0].files[0].size;
                            }
                        });

                        if (sum_files_size > _.post_max_size_bytes) {
                            $.ceNotification('show', {
                                type: 'E',
                                title: _.tr('error'),
                                message: _.tr('files_are_too_large').replace('[size]', _.post_max_size_mbytes)
                            });
                            this.clean_form();
                        }
                    }
                }
            }

            if (elm_id.indexOf('server') != -1) {
                _.fileuploader.init('box_server_upload', suffix);
            }

            if (elm_id.indexOf('url') != -1) {
                var e_url = $('#message_' + suffix + ' span').html();
                var n_url = '';

                if (n_url = prompt($('#' + elm_id).html(), (e_url.indexOf('://') !== -1) ? e_url : '')) {
                    this.display_filename(suffix, 'url', n_url);
                }
            }
        }
        _.fileuploader['display_filename'] = function(id, type, val) {
            // Highlight active link
            var types = ['local', 'server', 'url'];
            var file = $('#message_' + id + ' p.cm-fu-file');
            var no_file = $('#message_' + id + ' p.cm-fu-no-file');

            for (var i = 0; i < types.length; i++) {
                if (types[i] == type) {
                    $('#' + types[i] + '_' + id).addClass('active');
                } else {
                    $('#' + types[i] + '_' + id).removeClass('active');
                }
            }

            $('#type_' + id).val(type); // switch type
            $('#file_' + id).val(val); // set file name

            if (val == '') {
                file.hide();
                no_file.show();

                //Clear the input[type=file]
                var file_container = $('#link_container_' + id + ' > .upload-file-local');
                var content = file_container.html();
                file_container.html(content);

            } else {
                no_file.hide();

                if (type != 'url') {
                    var pieces = val.split(/(\\|\/)/g);
                    val = pieces[pieces.length - 1];
                }
                $('span', file).html(val).prop('title', val); // display file name
                file.show();
            }
        }
        $(document).on('change', '[name=link_category_id]', function() {
            var link_category_id = $(this);
            var category_id = link_category_id.val();
            if (category_id) {
                link_category_id.val('');
            }
            var category_link_id = $(this).attr('category_link_id');
            if (category_link_id && category_id) {
                if (category_link_id == 'input_checked') {
                    $('[name="category_link_ids[]"]:checked').each(function() {
                        link_category("xml_import.link_category", $(this).val(), category_id);
                    });
                } else {
                    link_category("xml_import.link_category", category_link_id, category_id);
                }
            }
        });
        $('#file_type').change(function() {
            if ($(this).val() == 'xml') {
                $('#div_csv_delimiter').hide();
            } else {
                $('#div_csv_delimiter').show();
            }
        });
        $('#file_type').change();


        if ($('.cm-js.active').attr('id') == 'category_mapping') {
            $('#btn_edit_category').css('visibility', 'visible');
        }

        $('.cm-js').click(function() {
            if ($(this).attr('id') == 'category_mapping') {
                $('#btn_edit_category').css('visibility', 'visible');
            } else {
                $('#btn_edit_category').css('visibility', 'hidden');    
            }
        });

        $('.fields').change(function() {
            $('#import_button').hide();
        });

        $('#opener_picker_categories_list_popup').click(function(){
            if (!$('[name="category_link_ids[]"]:checked').length) {
                $('.btn-group.dropleft').removeClass('open');
                fn_alert('{__("error_no_items_selected")|escape:"javascript"}');
                return false;
            }
        });

        $(document).on('click','.create-category', function(){
            $('#category_link_id').val($(this).attr('category_link_id'));
        });

        $('#form_create_category').submit(function() {
            var category_link_id = $('#category_link_id').val();
            if (category_link_id == 0) {
                category_link_id = '';
                $('[name="category_link_ids[]"]:checked').each(function() {
                    if (category_link_id) {
                        category_link_id += ',';
                    }
                    category_link_id += $(this).val()
                });
            }
            $('#create_category_link_id').val(category_link_id);
        });

        $('#filter_category').change(function() {
            $('tr.category').show();
            var value = $(this).val();
            if (value) {
                $('tr.category').hide();
                $('tr.category input[type="checkbox"]').attr('disabled','disabled');
                $('.' + value).parent().show();
                $('.' + value).parent().find('input[type="checkbox"]').removeAttr('disabled');
            } else {
                $('tr.category input[type="checkbox"]').removeAttr('disabled');
            }
            if ($('tr.category .not-category').parent(':visible').length) {
                $('#empty_tr').show();
            } else {
                $('#empty_tr').hide();
            }
        })

    });

    function link_category(url, category_link_id, category_id)
    {
        category_id = (typeof(category_id) == "undefined") ? 0 : category_id;
        $.ceAjax('request', fn_url(url), {
            method: 'post',
            data: {
                'import_profiles_id' : '{$smarty.request.import_profiles_id}',
                'category_link_id' : category_link_id,
                'category_id' : category_id,
                'result_ids' : 'tr-category-' + category_link_id,
            },
        });
    }

}(Tygh, Tygh.$));
</script>

{if $datafeed_data}
    {assign var="id" value=$datafeed_data.import_profiles_id}
{else}
    {assign var="id" value=0}
{/if}

<form action="{""|fn_url}" method="post" name="import_xml" enctype="multipart/form-data" class="form-horizontal form-edit">
<input type="hidden" class="cm-no-hide-input" name="import_profiles_id" value="{$datafeed_data.import_profiles_id}" />
<input type="hidden" class="cm-no-hide-input" name="datafeed_data[url]" value="{$datafeed_data.url}" />
<input type="hidden" name="selected_section" id="selected_section" value="{$smarty.request.selected_section}" />
<input type="hidden" id="category_link_id" value="" />


{capture name="tabsbox"}
    <div id="content_general">
        {include file="common/subheader.tpl" title=__("general_settings")}
        <div id="acc_information" class="collapse in">
            <div class="control-group">
                <label for="type_{"file[0]"|md5}" class="control-label {if !$datafeed_data.url}cm-required{/if}">{__("select_file")}:</label>
                    {if $datafeed_data.url}
                        <div class="controls">
                            {$datafeed_data.url}
                        </div>
                    {/if}
                <div id="elm_datafeed_file" class="controls">{include file="common/fileuploader.tpl"  var_name="file[0]" }</div>
            </div>

            <div class="control-group">
                <label class="control-label">{__("file_type")}:</label>
                <div class="controls">
                    <select id="file_type" name="datafeed_data[file_type]">
                        <option value="xml" {if $datafeed_data.file_type == "xml"}selected="selected"{/if}>{__("xml")}</option>
                        <option value="csv" {if $datafeed_data.file_type == "csv"}selected="selected"{/if}>{__("csv")}</option>
                    </select>
                </div>
            </div>

            <div class="control-group" id="div_csv_delimiter">
                <label class="control-label">{__("csv_delimiter")}:</label>
                <div class="controls">{include file="views/exim/components/csv_delimiters.tpl" name="datafeed_data[export_options][delimiter]" value="{$datafeed_data.export_options.delimiter}"}</div>
            </div>

            <div class="control-group">
                <label for="elm_datafeed_name" class="control-label cm-required">{__("name")}:</label>
                <div class="controls">
                    <input type="text" name="datafeed_data[datafeed_name]" id="elm_datafeed_name" size="55" value="{$datafeed_data.datafeed_name}" class="input-text-large main-input" />
                </div>
            </div>
            {if "MULTIVENDOR"|fn_allowed_for}
                {assign var="reload_form" value=true}
                {include file="views/companies/components/company_field.tpl"
                    name="datafeed_data[company_id]"
                    id="datafeed_data_company_id"
                    selected=$datafeed_data.company_id
                    reload_form=$reload_form
                }
            {/if}

            {if $pattern.options}
                {foreach from=$pattern.options key=k item=o}
                    {if !$o.import_only}
                        <div class="control-group">
                            <label for="elm_datafeed_element_{$p_id}_{$k}" class="control-label">{__($o.title)}:</label>
                            <div class="controls">
                                {if $o.type == "checkbox"}
                                    <input type="hidden" name="datafeed_data[export_options][{$k}]" value="N" />
                                    <input id="elm_datafeed_element_{$p_id}_{$k}" type="checkbox" name="datafeed_data[export_options][{$k}]" value="Y" {if $datafeed_data.export_options.$k == "Y"}checked="checked"{/if} />
                                {elseif $o.type == "input"}
                                    <input id="elm_datafeed_element_{$p_id}_{$k}" class="input-text-large" type="text" name="datafeed_data[export_options][{$k}]" value="{$datafeed_data.export_options.$k|default:$o.default_value}" />
                                {elseif $o.type == "languages"}
                                    <div class="checkbox-list">
                                        {foreach from=$export_langs item="language" key="lang_code"}
                                            <label>
                                                <input type="checkbox" name="datafeed_data[lang_code][]" value="{$lang_code}" {if $language.status == 'A'}checked="checked"{/if} />{$language.name}
                                            </label>
                                        {/foreach}
                                    </div>
                                {elseif $o.type == "select"}
                                    <select id="elm_datafeed_element_{$p_id}_{$k}" name="datafeed_data[export_options][{$k}]">
                                    {if $o.variants_function}
                                        {foreach from=$o.variants_function|call_user_func key=vk item=vi}
                                            <option value="{$vk}" {if $vk == $datafeed_data.export_options.$k|default:$o.default_value}checked="checked"{/if}> {$vi}</option>
                                        {/foreach}
                                    {else}
                                        {foreach from=$o.variants key=vk item=vi}
                                            <option value="{$vk}" {if $vk == $datafeed_data.export_options.$k|default:$o.default_value}checked="checked"{/if}>{__($vi)}</option>
                                        {/foreach}
                                    {/if}
                                    </select>
                                {/if}
                                {if $o.description}<p><small>{__($o.description)}</small></p>{/if}
                            </div>
                        </div>
                    {/if}
                {/foreach}
            {/if}

            <div class="control-group">
                <label for="elm_datafeed_file_name" class="control-label">{__("price_margin")}:</label>
                <div class="controls">
                    <input type="text" name="datafeed_data[price_margin]" value="{$datafeed_data.price_margin}" size="5" class="input-mini" />&nbsp;/&nbsp;
                    <select name="datafeed_data[type_price_margin]">
                        <option value="F" {if $datafeed_data.type_price_margin == 'F'} selected="selected" {/if}>{__("absolute")} ({$currencies.$primary_currency.symbol nofilter})</option>
                        <option value="P" {if $datafeed_data.type_price_margin == 'P'} selected="selected" {/if}>{__("percent")} (%)</option>
                    </select>
                </div>
            </div>
            {include file="common/select_status.tpl" input_name="datafeed_data[status]" id="elm_product_status" obj=$datafeed_data hidden=false}
        </div>
    </div>
    {if $datafeed_data.import_profiles_id}
        <div id="content_fields_mapping">
            {include file="addons/sd_xml_import/views/xml_import/components/import_fields.tpl"}
        </div>
    {/if}
    {if $categories}
        <div id="content_category_mapping">
            <div class="control-group">
                <label for="filter_category" class="control-label">{__("categories_filter")}:</label>
                <div class="controls">
                    <select id="filter_category">
                        <option value="">{__("all")}</option>
                        <option value="auto">{__("created_automatically")}</option>
                        <option value="manual">{__("created_manually")}</option>
                        <option value="not-category">{__("without_category")}</option>
                    </select>
                </div>
            </div>

            <table width="100%" class="table table-middle">
                <thead>
                    <tr>
                        <th class="left">
                            {include file="common/check_items.tpl" check_statuses=''|fn_get_default_status_filters:true}
                        </th>
                        <th width="40%">{__("category_from_xml")}</th>
                        <th width="40%">{__("store_category")}</th>
                        <th width="15%">{__("link_type")}</th>
                        <th width="5%"></th>
                    </tr>
                </thead>
                <tbody>
                    {if $categories['N']}
                        {foreach from=$categories['N'] item="category"}
                            {include file="addons/sd_xml_import/views/xml_import/components/category.tpl" category=$category}
                        {/foreach}
                    {/if}


                    {if $categories['Y']}
                        {if $categories['N']}
                            <tr id="empty_tr"><td colspan="5" align="center">&nbsp;</td></tr>
                        {/if}

                        {foreach from=$categories['Y'] item="category"}
                            {include file="addons/sd_xml_import/views/xml_import/components/category.tpl" category=$category}
                        {/foreach}
                    {/if}
                </tbody>
            </table>
        </div>
    {/if}
    {if $datafeed_data.fields}
        <div id="content_cron">
            {$id|fn_sd_xml_import_info_url nofilter}
        </div>
    {/if}
{/capture}

{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if $datafeed_data.import_profiles_id}
        {capture name="tools_list_edit"}
            <li>{include file="pickers/categories/picker.tpl" input_name="link_category_id" but_text="{__("edit_selected")}" view_mode="list" but_meta="icon" category_link_id="input_checked" rnd="popup"}</li>
            <li><a class="cm-dialog-opener cm-dialog-auto-size create-category" data-ca-target-id="category_path_popup" category_link_id="0">{__("create_category")}</a></li>
        {/capture}
        {dropdown content=$smarty.capture.tools_list_edit id="btn_edit_category"}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_name="dispatch[xml_import.update]" but_target_form="import_xml" but_meta="button_save" save=$datafeed_data.import_profiles_id}
        {if $datafeed_data.fields}
            {include file="buttons/button.tpl" but_text=__("import") but_id="import_button" but_name="dispatch[xml_import.run_import]" but_role="submit-link" but_target_form="import_xml" but_meta="cm-tab-tools"}
        {/if}
    {else}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="import_xml" but_name="dispatch[xml_import.update]"}
    {/if}
{/capture}
</form>
</div>
{/capture}

{if !$id}
    {assign var="title" value=__("new_import")}
{else}
    {assign var="title" value="{__("editing_import")}: `$datafeed_data.datafeed_name`"}
{/if}

{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
<div class="hidden" id="category_path_popup" title="{__('location_category')}">
    <form  action="{"xml_import.create_category"|fn_url}" name="create_category" id="form_create_category" method="post">
    <input type="hidden" name="import_profiles_id" value="{$smarty.request.import_profiles_id}">
    <input id="create_category_link_id" type="hidden" name="category_link_id" value="">

    <div id="location_category" class="controls">
        <select  id="elm_category_parent_id" name="category_id">
            <option value="0" {if $category_data.parent_id == "0"}selected="selected"{/if}>- {__("root_level")} -</option>
            {foreach from=0|fn_get_plain_categories_tree:false item="cat" name="categories"}
            {if !"ULTIMATE"|fn_allowed_for}
                <option value="{$cat.category_id}" {if $cat.disabled}disabled="disabled"{/if} {if $category_data.parent_id == $cat.category_id}selected="selected"{/if}>{$cat.category|escape|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;" nofilter}</option>
            {/if}
            {if "ULTIMATE"|fn_allowed_for}
                {if $cat.store}
                    {if !$smarty.foreach.categories.first}
                        </optgroup>
                    {/if}
                    <optgroup label="{$cat.category}">
                {else}
                    <option value="{$cat.category_id}" {if $cat.disabled}disabled="disabled"{/if} {if $category_data.parent_id == $cat.category_id}selected="selected"{/if}>{$cat.category|escape|indent:$cat.level:"&#166;&nbsp;&nbsp;&nbsp;&nbsp;":"&#166;--&nbsp;" nofilter}</option>
                {/if}
            {/if}
            {/foreach}
        </select>
    </div>
    <div class="buttons-container">
        <a class="cm-dialog-closer cm-cancel tool-link btn">{__("close")}</a>
        <input class="btn cm-process-items cm-dialog-closer btn-primary" id="btn_create_category" type="submit" value="{__("Create")}">
    </div>
    </form>
</div>