<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

$schema['bella_instagram'] = array (
    'content' => array (
        'items' => array (
            'remove_indent' => true,
            'hide_label' => true,
            'type' => 'enum',
            'object' => 'bella_instagram',
            'items_function' => 'fn_get_bella_instagram',
        ),
    ),
    'templates' => array (
        'addons/bella_instagram/blocks/original.tpl' => array(),
        'addons/bella_instagram/blocks/carousel.tpl' => array(
            'settings' => array (
                'navigation' => array (
                    'type' => 'selectbox',
                    'values' => array (
                        'N' => 'none',
                        'D' => 'dots',
                        'P' => 'pages',
                        'A' => 'arrows'
                    ),
                    'default_value' => 'D'
                ),
                'delay' => array (
                    'type' => 'input',
                    'default_value' => '3'
                ),
            ),
        )
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'update_handlers' => array(
            'bella_instagram', 'bella_instagram_descriptions', 'bella_instagram_images'
        )
    ),
    'brief_info_function' => 'fn_block_get_block_with_items_info'
);

return $schema;
