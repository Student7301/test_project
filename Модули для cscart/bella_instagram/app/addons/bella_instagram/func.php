<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;
use Tygh\Tools\SecurityHelper;

use Tygh\Http;


if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_bella_instagram() {
	$received = Http::get('https://www.instagram.com/bellecannelle/?__a=1');
	$json = json_decode($received);

	foreach ($json->graphql->user->edge_owner_to_timeline_media->edges as $key => $value) {
		$data[] = [
			'img' => $value->node->display_url,
			'href' => 'https://www.instagram.com/p/' . $value->node->shortcode . '/',
			'text' => $value->node->edge_media_to_caption->edges[0]->node->text
		];
	}	

	return $data;
} 