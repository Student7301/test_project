
{assign var="value" value=$user_data|fn_get_bella_instagram}

<div class="carousel-container">
    <div id="slider-carousel" class="owl-carousel">
	    {foreach from=$value item="inst" key="key"}
  	    <div class="item">
              <a class="hoverfx" href="{$inst.href}">
                  <div class="figure">
                      Подробнее
                  </div>
                  <div class="overlay">
                  </div>
                  <img src="{$inst.img}"/>
              </a>
          </div>
  	  {/foreach}
    </div>

    <div class="customNavigation">
        <a class="btn gray prev"><i class="icon ion-ios-arrow-left">Назад</i></a>
        <a class="btn gray next"><i class="icon ion-ios-arrow-right">Вперед</i></a>
    </div>
</div> 