<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="lunch-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="lunch-header__image">
                    <img src="assets/img/ra-logo.png" alt="">
                </div>
                <div class="lunch-title"><?=$arResult['NAME'];?></div>
            </div>
            <div class="col-12">
                <div class="lunch-header__food">
                    <?foreach ($arResult['ITEMS'] as $img_element):?>
                        <img src="<?=$img_element['DETAIL_PICTURE']['SRC'];?>" alt="<?=$img_element['NAME'];?>">
                    <?endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>